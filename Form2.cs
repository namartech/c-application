﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FormApplication
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            Fill();
        }

        void Fill()
        {
            string constring = "datasource=localhost;port=3306;username=root;password=root";
            string Query = "select * from database.visitors ;";

            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                while (myReader.Read())
                {
                    string sName = myReader.GetString("name");
                    comboBox1.Items.Add(sName);
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string constring = "datasource=localhost;port=3306;username=root;password=root";
            string Query = "insert into database.visitors (idvisitors,name,surname,age) values('" + this.Eid_txt.Text + "','" + this.Name_txt.Text + "','" + this.Surname_txt.Text + "','" + this.Age_txt.Text + "');";

            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("Saved");
                while (myReader.Read())
                {
                }

            } catch(Exception ex) {

                MessageBox.Show(ex.Message);
            }
          }

        private void button2_Click(object sender, EventArgs e)
        {
             string constring = "datasource=localhost;port=3306;username=root;password=root";
             string Query = "update database.visitors set idvisitors='" + this.Eid_txt.Text + "',name='" + this.Name_txt.Text + "',surname='" + this.Surname_txt.Text + "',age='" + this.Age_txt.Text + "' where idvisitors='" + this.Eid_txt.Text + "' ;";

            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("Updated");
                while (myReader.Read())
                {
                }

            } catch(Exception ex) {

                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
                 string constring = "datasource=localhost;port=3306;username=root;password=root";
                 string Query = "delete from database.visitors where idvisitors='" + this.Eid_txt.Text + "';";

            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                MessageBox.Show("Deleted");
                while (myReader.Read())
                {
                }

            } catch(Exception ex) {

                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string namestr=Name_txt.Text;
            comboBox1.Items.Add(namestr);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string constring = "datasource=localhost;port=3306;username=root;password=root";
            string Query = "select * from database.visitors where name='"+ comboBox1.Text +"' ;";

            MySqlConnection conDataBase = new MySqlConnection(constring);
            MySqlCommand cmdDataBase = new MySqlCommand(Query, conDataBase);
            MySqlDataReader myReader;
            try
            {
                conDataBase.Open();
                myReader = cmdDataBase.ExecuteReader();
                while (myReader.Read())
                {
                    string sIdvisitors = myReader.GetInt32("idvisitors").ToString();
                    string sName = myReader.GetString("name");
                    string sSurname = myReader.GetString("surname");
                    string sAge = myReader.GetInt32("age").ToString();

                    Eid_txt.Text = sIdvisitors;
                    Name_txt.Text = sName;
                    Surname_txt.Text = sSurname;
                    Age_txt.Text = sAge;
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
